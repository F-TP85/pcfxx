import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
const Directives =  () => import(/* webpackChunkName: 'ImportFuncDemo' */ '../components/Directives.vue')
const SelectAddress =  () => import(/* webpackChunkName: 'ImportFuncDemo' */ '../components/SelectAddress.vue')

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    redirect: '/directives',
    children: [
      {
        path: '/directives',
        name: 'directives',
        component: Directives
      },
      {
        path: '/SelectAddress',
        name: 'SelectAddress',
        component: SelectAddress
      },
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
