// 自定义指令就一个对象
export default {
    // 当元素插入到DOM的时候
    inserted(el, binding, vnode) {
        let bueVal = binding.value
        let butBoolean = vnode.context.$store.state.directives.buttonPermission[bueVal]
        // console.log(bueVal, butBoolean);
        !butBoolean && el.parentNode.removeChild(el); //不满足条件则把按钮删掉
    }
}