const state = {
   buttonPermission: {
       add: true,
       edit: true,
       delete: false
   }
};
const mutations = {
    // TOGGLE_SIDEBAR: (state) => {
      
    // }
};
const actions = {
    // toggleSideBar({ commit }) {
    //     commit("TOGGLE_SIDEBAR");
    // }
};
export default {
    namespaced: true,
    state,
    mutations,
    actions
};